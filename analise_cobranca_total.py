from functions import *
from qry_cobranca_total import *
from datetime import datetime
import sqlalchemy

# Parametro de conexão com o dw.
parametros = get_parameters()
user_dw = parametros['dw.db']['user']
psw_dw = parametros['dw.db']['pass']
host_dw = parametros['dw.db']['host']

# Parametro de conexão com o banco de produção
user_prdme = parametros['prdme.db']['user']
psw_prdme = parametros['prdme.db']['pass']
host_prdme = parametros['prdme.db']['host']

# Engine do banco DW
engine_prd = get_engine_prdme()
engine_dw = get_engine_dw()

# 1 - Buscar informaçoes dos últimos 13 meses
with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
    df_calendar = pd.read_sql(ultimos_13_meses, connection)

df_calendar = df_calendar.sort_values(by=['ID'], ascending=True)
df_13_mes = pd.DataFrame()
for i, r in df_calendar.iterrows():

    with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
        print('Buscando registro da competencia ', r[3])
        df_13_mes = df_13_mes.append(pd.read_sql(sql_cobranca_total.format(r[4] + r[5], r[3]), connection))

# 2 - Deletar as informações dos ultimos 13 meses do dw
print('Deletando as informações dos ultimos 13 meses')
with engine_prd.connect() as con:
    con.execute(del_13mes_cobranca_total)
print('Deletado')

# 3 -  salvar as informações dos últimos 13 meses no DW
print('Salvando os registros dos últimos 13 meses')
df_13_mes.to_sql('analise_cobranca_total',
                 con=engine_prd,
                 if_exists='append',
                 index=False,
                 dtype={
                     'QUEBRA': sqlalchemy.types.NVARCHAR(length=255),
                     'COMPETENCIA': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_ABERTO': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_ABERTO_VENCIDO': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_VALOR_ABERTO_FIM_MES': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_VALOR_ABERTO_ACUMULADO': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_ANTES': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_NO_MES':  sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_INADIMPLENCIA': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_30_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_60_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_90_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_120_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_150_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_180_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                     'PORC_PAGO_COM_MAIS_180_DIAS': sqlalchemy.types.NVARCHAR(length=255)}
                 )
print('Registros salvos')

# 4 - Se domingo
if datetime.today().weekday() == 6:

    # 4.2 - buscar registro entre 10/2018 até   13 meses pra trás
    with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
        df_calendar = pd.read_sql(anterior_a_13_meses, connection)

    df_calendar = df_calendar.sort_values(by=['ID'], ascending=True)
    df_ant_13_mes = pd.DataFrame()
    for i, r in df_calendar.iterrows():
        print('Buscando registro da competencia ', r[3])
        with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
            df_ant_13_mes = df_ant_13_mes.append(pd.read_sql(sql_cobranca_total.format(r[4] + r[5], r[3]), connection))

    # 4.3 - Deletar registro entre 10/2018 até   13 meses pra trás
    print('Deletando registro entre 10/2018 até   13 meses pra trás')
    with engine_prd.connect() as con:
        con.execute(del_ant_13mes_cobranca_total)
    print('deletado')

    # 4.4 - Salvar no DW
    print('Salvando os registros do periodo anterior a 13 meses')
    df_ant_13_mes.to_sql('analise_cobranca_total', con=engine_prd, if_exists='append', index=False,
                         dtype={
                             'QUEBRA': sqlalchemy.types.NVARCHAR(length=255),
                             'COMPETENCIA': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_ABERTO': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_ABERTO_VENCIDO': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_VALOR_ABERTO_FIM_MES': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_VALOR_ABERTO_ACUMULADO': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_ANTES': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_NO_MES': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_INADIMPLENCIA': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_30_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_60_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_90_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_120_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_150_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_180_DIAS': sqlalchemy.types.NVARCHAR(length=255),
                             'PORC_PAGO_COM_MAIS_180_DIAS': sqlalchemy.types.NVARCHAR(length=255)}
                         )
    print('Registros salvos')
    data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
    data = datetime.strptime(data_execucao, '%d/%m/%Y %H:%M:00')
    with engine_dw.connect() as con:
        con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'cobranca_total_ant13mes' ''')
        con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'cobranca_total_ant13mes')'''.format(data))

data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'cobranca_total13m' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'cobranca_total13m')'''.format(data))

