sql_controle_recebimento ='''
SELECT W.COMPETENCIA                                                                                                         AS "COMPETENCIA",
       TO_DATE(W.COMPETENCIA, 'MM/YYYY')                                                                                     AS "DT_COMPETENCIA",
       W.TOTAL_EMITIDO_C_CANCELADO                                                                                           AS "TOTAL_EMITIDO_C_CANCELADO",
       W.TOTAL_EMITIDO_CANCELADO                                                                                             AS "TOTAL_EMITIDO_CANCELADO",
       W.TOTAL_EMITIDO                                                                                                       AS "TOTAL_EMITIDO",
       W.QTD_TOTAL_EMITIDO                                                                                                   AS "QTD_TOTAL_EMITIDO",
       W.TOTAL_PAGO                                                                                                          AS "TOTAL_PAGO",
       W.TOTAL_TITULOS_PAGOS                                                                                                 AS "TOTAL_TITULOS_PAGOS",
       W.VL_ANTECIPADO                                                                                                       AS "VL_ANTECIPADO",
       W.QTD_VL_ANTECIPADO                                                                                                   AS "QTD_VL_ANTECIPADO",
       W.VL_DENTRO_VENCIMENTO                                                                                                AS "VL_DENTRO_VENCIMENTO",
       W.QTD_VL_DENTRO_VENCIMENTO                                                                                            AS "QTD_VL_DENTRO_VENCIMENTO",
       W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO                                                                              AS "VL_LIQUIDEZ",
       CASE
           WHEN W.TOTAL_EMITIDO = 0 THEN 0
           ELSE
               ROUND(((W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO) / W.TOTAL_EMITIDO), 2)
           END                                                                                                               AS "LIQUIDEZ",

       W.VL_DENTRO_COMPETENCIA                                                                                               AS "VL_DENTRO_COMPETENCIA",
       W.QTD_VL_DENTRO_COMPETENCIA                                                                                           AS "QTD_VL_DENTRO_COMPETENCIA",
       W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO + W.VL_DENTRO_COMPETENCIA                                                    AS "VL_LIQUIDEZ_DENTRO_COMPETENCIA",

       CASE
           WHEN W.TOTAL_EMITIDO = 0 THEN 0
           ELSE
               ROUND(
                       ((W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO + W.VL_DENTRO_COMPETENCIA) /
        W.TOTAL_EMITIDO),2) END AS "LIQUIDEZ_DENTRO_COMPETENCIA",

       W.ABERTO_VENCIDO_FIM_MES                                                                                              AS "ABERTO_VENCIDO_FIM_MES",
       W.QTD_ABERTO_VENCIDO_FIM_MES                                                                                          AS "QTD_ABERTO_VENCIDO_FIM_MES",

       CASE
           WHEN W.TOTAL_EMITIDO = 0 THEN 0
           ELSE
               ROUND((W.ABERTO_VENCIDO_FIM_MES / W.TOTAL_EMITIDO), 2) END                                                    AS "INAD_VENCIDO_FIM_MES",

       W.ABERTO_VENCIDO_HJ                                                                                                   AS "ABERTO_VENCIDO_HJ",
       W.QTD_ABERTO_VENCIDO_HJ                                                                                               AS "QTD_ABERTO_VENCIDO_HJ",

       CASE
           WHEN W.TOTAL_EMITIDO = 0 THEN 0
           ELSE
               ROUND((W.ABERTO_VENCIDO_HJ / W.TOTAL_EMITIDO), 2) END                                                         AS "INAD_ABERTO_VENCIDO_HJ",

       W.VL_PAGO_30_DIAS                                                                                                     AS "VL_PAGO_30_DIAS",
       W.QTD_VL_PAGO_30_DIAS                                                                                                 AS "QTD_VL_PAGO_30_DIAS",


       (W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO
           + W.VL_DENTRO_COMPETENCIA +
        W.VL_PAGO_30_DIAS)                                                                                                   AS "VL_LIQUIDEZ_PAGO_30_DIAS",


       CASE
           WHEN W.TOTAL_EMITIDO = 0 THEN 0
           ELSE
               ROUND(
                       ((W.VL_ANTECIPADO + W.VL_DENTRO_VENCIMENTO
                           + W.VL_DENTRO_COMPETENCIA + W.VL_PAGO_30_DIAS) /
        W.TOTAL_EMITIDO),2) END                    AS "LIQUIDEZ_PAGO_30_DIAS",

       W.VL_PAGO_60_DIAS                                                                                                     AS "VL_PAGO_60_DIAS",
       W.QTD_VL_PAGO_60_DIAS                                                                                                 AS "QTD_VL_PAGO_60_DIAS",
       W.VL_PAGO_90_DIAS                                                                                                     AS "VL_PAGO_90_DIAS",
       W.QTD_VL_PAGO_90_DIAS                                                                                                 AS "QTD_VL_PAGO_90_DIAS",
       W.VL_PAGO_120_DIAS                                                                                                    AS "VL_PAGO_120_DIAS",
       W.QTD_VL_PAGO_120_DIAS                                                                                                AS "QTD_VL_PAGO_120_DIAS",
       W.VL_PAGO_150_DIAS                                                                                                    AS "VL_PAGO_150_DIAS",
       W.QTD_VL_PAGO_150_DIAS                                                                                                AS "QTD_VL_PAGO_150_DIAS",
       W.VL_PAGO_180_DIAS                                                                                                    AS "VL_PAGO_180_DIAS",
       W.QTD_VL_PAGO_180_DIAS                                                                                                AS "QTD_VL_PAGO_180_DIAS",
       W.VL_PAGO_MAIS_180                                                                                                    AS "VL_PAGO_MAIS_180",
       W.QTD_VL_PAGO_MAIS_180                                                                                                AS "QTD_VL_PAGO_MAIS_180",

       (W.VL_PAGO_30_DIAS + W.VL_PAGO_60_DIAS + W.VL_PAGO_90_DIAS +
        W.VL_PAGO_120_DIAS + W.VL_PAGO_150_DIAS + W.VL_PAGO_180_DIAS +
        W.VL_PAGO_MAIS_180)                                                                                                  AS "TOTAL_EM_ATRASO",

       CASE
           WHEN W.TOTAL_PAGO = 0 THEN 0
           ELSE
               ROUND((
                   (W.VL_PAGO_30_DIAS + W.VL_PAGO_60_DIAS + W.VL_PAGO_90_DIAS +
                    W.VL_PAGO_120_DIAS + W.VL_PAGO_150_DIAS + W.VL_PAGO_180_DIAS +
                    W.VL_PAGO_MAIS_180) /

         W.TOTAL_PAGO),2) END                                                        AS "INDICE_COBRANCA",
       SYSDATE                                                                                                               AS "DATA_EXECUCAO"


FROM (
         SELECT X.COMP                              AS "COMPETENCIA",
                ------
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                        /* AND A.CD_MOTIVO_CANCELAMENTO IS NULL */ ), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                        /* AND A.CD_MOTIVO_CANCELAMENTO IS NULL */ ), 0)
                                                    AS "TOTAL_EMITIDO_C_CANCELADO",
                -----
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)
                                                    AS "TOTAL_EMITIDO_CANCELADO",
                -----
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                                                    AS "TOTAL_EMITIDO",
                -----

                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                                                    AS "QTD_TOTAL_EMITIDO",
                -----
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "TOTAL_PAGO",
                -----
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "TOTAL_TITULOS_PAGOS",
                -----
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) < TRUNC(M.DT_VENCIMENTO)
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_ANTECIPADO",
                -----
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) < TRUNC(M.DT_VENCIMENTO)
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_ANTECIPADO",
                -----
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) = TRUNC(M.DT_VENCIMENTO)
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_DENTRO_VENCIMENTO",
                ------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) = TRUNC(M.DT_VENCIMENTO)
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_DENTRO_VENCIMENTO",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') = X.COMP
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_DENTRO_COMPETENCIA",
                ------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') = X.COMP
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_DENTRO_COMPETENCIA",
                ------
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0) AS "ABERTO_VENCIDO_FIM_MES",
                ------
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0) AS "QTD_ABERTO_VENCIDO_FIM_MES",
                ------
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0) AS "ABERTO_VENCIDO_HJ",
                ------
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0) AS "QTD_ABERTO_VENCIDO_HJ",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 0 AND 30
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_30_DIAS",
                ------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 0 AND 30
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_30_DIAS",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 31 AND 60
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_60_DIAS",
                -------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 31 AND 60
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_60_DIAS",
                -------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 61 AND 90
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_90_DIAS",
                -------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 61 AND 90
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_90_DIAS",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 91 AND 120
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_120_DIAS",
                ------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 91 AND 120
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_120_DIAS",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 121 AND 150
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_150_DIAS",
                -----
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 121 AND 150
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_150_DIAS",
                ------
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 151 AND 180
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_180_DIAS",
                ------
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) BETWEEN 151 AND 180
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_180_DIAS",
                -----
                NVL(
                        (SELECT SUM(Y.VL_RECEBIMENTO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) > 180
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL
                           AND M.CD_REPACTUACAO IS NULL)
                    , 0)                            AS "VL_PAGO_MAIS_180",
                -----
                NVL(
                        (SELECT COUNT(M.CD_MENS_CONTRATO)
                         FROM DBAPS.MENS_CONTRATO_REC Y
                                  INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_MENS_CONTRATO = Y.CD_MENS_CONTRATO
                         WHERE TO_CHAR(Y.DT_RECEBIMENTO, 'MM/YYYY') = X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) > TRUNC(M.DT_VENCIMENTO)
                           AND TO_CHAR(TRUNC(M.DT_VENCIMENTO), 'MM/YYYY') <> X.COMP
                           AND TRUNC(Y.DT_RECEBIMENTO) - TRUNC(M.DT_VENCIMENTO) > 180
                           AND Y.DT_ESTORNO IS NULL
                           AND M.CD_MOTIVO_CANCELAMENTO IS NULL)
                    , 0)                            AS "QTD_VL_PAGO_MAIS_180"

         FROM (
                  SELECT DISTINCT M.NR_MES || '/' || M.NR_ANO AS "COMP"
                  FROM DBAPS.MENS_CONTRATO M
                  WHERE M.CD_MOTIVO_CANCELAMENTO IS NULL
                  AND M.NR_ANO || M.NR_MES = SUBSTR(('{1}'), 4, 7) || SUBSTR(('{1}'), 0, 2)
 -- AND M.NR_ANO || M.NR_MES <= SUBSTR(($CompetenciaFinalCobranca$) ,4,7)  || SUBSTR(($CompetenciaFinalCobranca$) ,0,2) */
                    AND TO_NUMBER(M.NR_ANO || M.NR_MES) = {0}) X) W
'''



ultimos_13_meses = '''select * from CALENDARIO_BI C
WHERE C.PRIMEIRO_DIA_MES <= TO_DATE(add_months(SYSDATE,5),'DD/MM/YY')
and trunc(C.PRIMEIRO_DIA_MES) >= add_months(trunc( last_day(sysdate)+1),- 13) '''


anterior_a_13_meses = '''
select * from CALENDARIO_BI C
WHERE
      C.PRIMEIRO_DIA_MES >= TO_DATE('01/10/2018','DD/MM/YY')
and trunc(C.PRIMEIRO_DIA_MES) < add_months(trunc( last_day(sysdate)+1),- 13)
'''




del_13mes = '''
DELETE
    FROM dbaps.controle_recebimento_bi R
    WHERE
        r.DT_COMPETENCIA >= last_day(add_months(trunc(sysdate),- 13) ) +  1 and r.DT_COMPETENCIA <= trunc(sysdate)
        '''


del_ant_13mes = '''
DELETE
    FROM dbaps.controle_recebimento_bi R
    WHERE
        r.DT_COMPETENCIA < last_day(add_months(trunc(sysdate),- 13) ) +  1
        '''

