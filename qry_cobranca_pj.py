sql_cobranca_pj ='''
SELECT 'PJ'                                                                                                    AS "QUEBRA",
       RESUMO.COMPETENCIA,
       TO_DATE(TO_CHAR('01' || SUBSTR((RESUMO.COMPETENCIA), 0, 2) || SUBSTR((RESUMO.COMPETENCIA), 4, 7)),
               'DD/MM/YYYY')                                                                                   AS "DT_COMPETENCIA",
       (SELECT COUNT(Q.CD_MATRICULA)
        FROM DBAPS.USUARIO Q
                 INNER JOIN DBAPS.CONTRATO QC ON QC.CD_CONTRATO = Q.CD_CONTRATO
        WHERE TRUNC(Q.DT_CADASTRO) <= LAST_DAY(TO_DATE('01/' || RESUMO.COMPETENCIA, 'DD/MM/YYYY'))
          AND NOT EXISTS(SELECT 1
                         FROM DBAPS.DESLIGAMENTO D
                         WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                           AND D.DT_REATIVACAO IS NULL
                           AND TRUNC(D.DT_DESLIGAMENTO) <=
                               LAST_DAY(TO_DATE('01/' || RESUMO.COMPETENCIA, 'DD/MM/YYYY'))))                  AS "ATIVOS_FINAL_DO_MES",
       RESUMO.TOTAL_PAGO_JOIN                                                                                  AS "VALOR_TOTAL_PAGO",
       RESUMO.TOTAL                                                                                            AS "VALOR_TOTAL_EMITIDO",
       RESUMO.NTOTAL                                                                                           AS "QTE_TOTAL_TITULOS",
       RESUMO.ABERTO                                                                                           AS "VALOR_ABERTO",
       CAST(((RESUMO.ABERTO / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_ABERTO",
       RESUMO.ABERTO_VENCIDO                                                                                   AS "VALOR_ABERTO_VENCIDO",
       CAST(((RESUMO.ABERTO_VENCIDO / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_ABERTO_VENCIDO",
       RESUMO.ABERTO_FIM_MES                                                                                   AS "VALOR_ABERTO_FIM_MES",
       CASE
           WHEN RESUMO.TOTAL = 0 THEN '0 %'
           ELSE
                   TO_CHAR(ROUND(((RESUMO.ABERTO_FIM_MES / RESUMO.TOTAL) * 100), 2)) ||
                   ' %' END                                                                                    AS "PORC_VALOR_ABERTO_FIM_MES",
       RESUMO.ABERTO_ACUMULADO                                                                                 AS "VALOR_ABERTO_ACUMULADO",
       CASE
           WHEN RESUMO.TOTAL = 0 THEN '0 %'
           ELSE
                   TO_CHAR(ROUND(((RESUMO.ABERTO_ACUMULADO / RESUMO.TOTAL) * 100), 2)) ||
                   ' %' END                                                                                    AS "PORC_VALOR_ABERTO_ACUMULADO",
       RESUMO.NABERTO                                                                                          AS "QTE_TITULOS_ABERTOS",
       RESUMO.ANTES                                                                                            AS "VALOR_PAGO_ANTES",
       CAST(((RESUMO.ANTES / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_ANTES",
       RESUMO.NANTES                                                                                           AS "QTE_TITULOS_PAGO_ANTES",
       RESUMO.MES                                                                                              AS "VALOR_NO_MES",
       CAST(((RESUMO.MES / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_NO_MES",
       RESUMO.NMES                                                                                             AS "QTE_TITULOS_PAGO_MES",
       CAST((((RESUMO.TOTAL - (RESUMO.MES + RESUMO.ANTES)) / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) *
             100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_INADIMPLENCIA",
       (RESUMO.TOTAL - (RESUMO.MES + RESUMO.ANTES))                                                            AS "VALOR_INADIMPLENCIA",
       RESUMO.A30                                                                                              AS "VALOR_PAGO_COM_30",
       CAST(((RESUMO.A30 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_30_DIAS",
       RESUMO.NA30                                                                                             AS "QTE_TITULOS_PAGO_30",
       RESUMO.A60                                                                                              AS "VALOR_PAGO_COM_60",
       CAST(((RESUMO.A60 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_60_DIAS",
       RESUMO.NA60                                                                                             AS "QTE_TITULOS_PAGO_60",
       RESUMO.A90                                                                                              AS "VALOR_PAGO_COM_90",
       CAST(((RESUMO.A90 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_90_DIAS",
       RESUMO.NA90                                                                                             AS "QTE_TITULOS_PAGO_90",
       RESUMO.A120                                                                                             AS "VALOR_PAGO_COM_120",
       CAST(((RESUMO.A120 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_120_DIAS",
       RESUMO.NA120                                                                                            AS "QTE_TITULOS_PAGO_120",
       RESUMO.A150                                                                                             AS "VALOR_PAGO_COM_150",
       CAST(((RESUMO.A150 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_150_DIAS",
       RESUMO.NA180                                                                                            AS "QTE_TITULOS_PAGO_150",
       RESUMO.A180                                                                                             AS "VALOR_PAGO_COM_180",
       CAST(((RESUMO.A180 / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_180_DIAS",
       RESUMO.NA180                                                                                            AS "QTE_TITULOS_PAGO_180",
       RESUMO.MAIS                                                                                             AS "VALOR_PAGO_COM_MAIS_180",
       CAST(((RESUMO.MAIS / DECODE(RESUMO.TOTAL, 0, 1, RESUMO.TOTAL)) * 100) AS NUMBER(6, 2)) ||
       '%'                                                                                                     AS "PORC_PAGO_COM_MAIS_180_DIAS",
       RESUMO.NMAIS                                                                                            AS "QTE_TITULOS_PAGO_MAIS_180",
       RESUMO.TOTAL_CANCELADO                                                                                  AS "TOTAL_CANCELADO",
       RESUMO.N_TOTAL_CANCELADO                                                                                AS "N_TOTAL_CANCELADO",
       SYSDATE                                                                                                 AS "DATA_EXECUCAO"
FROM (
         SELECT X.COMP                                                                     AS "COMPETENCIA",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)                           AS "TOTAL",
                --
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)                           AS "NTOTAL",
                --
                NVL((SELECT SUM(
                                    CASE
                                        WHEN A.CD_REPACTUACAO IS NOT NULL THEN 0
                                        ELSE
                                            (SELECT SUM(R.VL_RECEBIDO)
                                             FROM DBAMV.RECCON_REC R
                                                      INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                                                      INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_CON_REC = P.CD_CON_REC
                                                      INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = M.CD_CONTRATO
                                             WHERE M.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                                               AND R.DT_ESTORNO IS NULL) END)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)
                    +
                NVL((SELECT SUM(
                                    CASE
                                        WHEN A.CD_REPACTUACAO IS NOT NULL THEN 0
                                        ELSE
                                            (SELECT SUM(R.VL_RECEBIDO)
                                             FROM DBAMV.RECCON_REC R
                                                      INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                                                      INNER JOIN DBAPS.MENS_CONTRATO M ON M.CD_CON_REC = P.CD_CON_REC
                                                      INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = M.CD_CONTRATO
                                             WHERE M.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                                               AND R.DT_ESTORNO IS NULL) END)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL), 0)                           AS "TOTAL_PAGO_SUB",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810), 0)              AS "TOTAL_PAGO_JOIN",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810), 0)              AS "N_TOTAL_PAGO_JOIN",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TRUNC(A.DT_VENCIMENTO) < TRUNC(SYSDATE)
                       AND A.TP_QUITACAO = 'A'), 0)                                        AS "ABERTO_VENCIDO",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)                                        AS "ABERTO",
                --
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)                                        AS "NABERTO",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                              LEFT JOIN DBAPS.MENS_CONTRATO_REC MCR ON MCR.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                         /* AND A.TP_QUITACAO = 'A' */
                       AND MCR.DT_ESTORNO IS NULL
                       AND (MCR.DT_RECEBIMENTO >=
                            LAST_DAY(TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY'))
                         OR MCR.DT_RECEBIMENTO IS NULL)), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                              LEFT JOIN DBAPS.MENS_CONTRATO_REC MCR ON MCR.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                         /* AND A.TP_QUITACAO = 'A' */
                       AND MCR.DT_ESTORNO IS NULL
                       AND (MCR.DT_RECEBIMENTO >=
                            LAST_DAY(TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY'))
                         OR MCR.DT_RECEBIMENTO IS NULL)), 0)                               AS "ABERTO_FIM_MES",
                --
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                              LEFT JOIN DBAPS.MENS_CONTRATO_REC MCR ON MCR.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                         /* AND A.TP_QUITACAO = 'A' */
                       AND MCR.DT_ESTORNO IS NULL
                       AND (MCR.DT_RECEBIMENTO >=
                            LAST_DAY(TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY'))
                         OR MCR.DT_RECEBIMENTO IS NULL)), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                              LEFT JOIN DBAPS.MENS_CONTRATO_REC MCR ON MCR.CD_MENS_CONTRATO = A.CD_MENS_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                         /* AND A.TP_QUITACAO = 'A' */
                       AND MCR.DT_ESTORNO IS NULL
                       AND (MCR.DT_RECEBIMENTO >=
                            LAST_DAY(TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY'))
                         OR MCR.DT_RECEBIMENTO IS NULL)), 0)                               AS "N_ABERTO_FIM_MES",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_DATE(A.NR_MES || '/' || A.NR_ANO, 'MM/YYYY') >= TO_DATE('10/2018', 'MM/YYYY')
                       AND TO_DATE(A.NR_MES || '/' || A.NR_ANO, 'MM/YYYY') <= TO_DATE(X.COMP, 'MM/YYYY')
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY') >=
                           TO_DATE('10/2018', 'MM/YYYY')
                       AND TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY') <=
                           TO_DATE(X.COMP, 'MM/YYYY')
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)                                        AS "ABERTO_ACUMULADO",
                --
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_DATE(A.NR_MES || '/' || A.NR_ANO, 'MM/YYYY') >= TO_DATE('10/2018', 'MM/YYYY')
                       AND TO_DATE(A.NR_MES || '/' || A.NR_ANO, 'MM/YYYY') <= TO_DATE(X.COMP, 'MM/YYYY')
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY') >=
                           TO_DATE('10/2018', 'MM/YYYY')
                       AND TO_DATE(TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY'), 'MM/YYYY') <=
                           TO_DATE(X.COMP, 'MM/YYYY')
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND A.TP_QUITACAO = 'A'), 0)                                        AS "N_ABERTO_ACUMULADO",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) < TRUNC(MA.DT_VENCIMENTO_ORIGINAL)), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) < TRUNC(MA.DT_VENCIMENTO_ORIGINAL)), 0) AS "ANTES",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) < TRUNC(MA.DT_VENCIMENTO_ORIGINAL)), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) < TRUNC(MA.DT_VENCIMENTO_ORIGINAL)), 0) AS "NANTES",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') = X.COMP), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') = X.COMP), 0)
                       
                       +
                       case when SUBSTR(X.COMP,4,4)||SUBSTR(X.COMP,1,2) >= SUBSTR('01/2022',4,4)||SUBSTR('01/2022',1,2)
                        then (SELECT DBAPS.FNC_RET_VL_ADIPLENTE_PFPJ(X.COMP,'PJ') FROM DUAL)
                        else 0 end
                              
                              AS "MES",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') = X.COMP), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') = X.COMP), 0)       AS "NMES",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 0 AND 30
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 0 AND 30
                    ), 0)                                                                  AS "A30",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 0 AND 30
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 0 AND 30
                    ), 0)                                                                  AS "NA30",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 31 AND 60
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 31 AND 60
                    ), 0)                                                                  AS "A60",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 31 AND 60
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 31 AND 60
                    ), 0)                                                                  AS "NA60",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 61 AND 90
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 61 AND 90
                    ), 0)                                                                  AS "A90",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 61 AND 90
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 61 AND 90
                    ), 0)                                                                  AS "NA90",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 91 AND 120
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 91 AND 120
                    ), 0)                                                                  AS "A120",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 91 AND 120
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 91 AND 120
                    ), 0)                                                                  AS "NA120",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 121 AND 150
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 121 AND 150
                    ), 0)                                                                  AS "A150",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 121 AND 150
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 121 AND 150
                    ), 0)                                                                  AS "NA150",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 151 AND 180
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 151 AND 180
                    ), 0)                                                                  AS "A180",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 151 AND 180
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) BETWEEN 151 AND 180
                    ), 0)                                                                  AS "NA180",
                --
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) > 180
                    ), 0)
                    +
                NVL((SELECT SUM(CASE
                                    WHEN MA.CD_REPACTUACAO IS NOT NULL THEN 0
                                    ELSE R.VL_RECEBIDO END) AS "VL_RECEBIDO"
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) > 180
                    ), 0)                                                                  AS "MAIS",
                --
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE <> 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND MA.NR_MES || '/' || MA.NR_ANO = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) > 180
                    ), 0)
                    +
                NVL((SELECT COUNT(MA.CD_MENS_CONTRATO)
                     FROM DBAMV.RECCON_REC R
                              INNER JOIN DBAMV.ITCON_REC P ON P.CD_ITCON_REC = R.CD_ITCON_REC
                              INNER JOIN DBAPS.MENS_CONTRATO MA ON MA.CD_CON_REC = P.CD_CON_REC
                              INNER JOIN DBAPS.CONTRATO MC ON MC.CD_CONTRATO = MA.CD_CONTRATO
                     WHERE MC.TP_MENSALIDADE = 'O'
                       AND MC.TP_CONTRATO <> 'I'
                       AND R.DT_ESTORNO IS NULL
                       AND MA.CD_MOTIVO_CANCELAMENTO IS NULL
                       AND TO_CHAR(MA.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND TO_NUMBER(MA.NR_ANO || MA.NR_MES) >= 201810
                       AND TRUNC(R.DT_RECEBIMENTO) >= TRUNC(MA.DT_VENCIMENTO_ORIGINAL)
                       AND TO_CHAR(TRUNC(R.DT_RECEBIMENTO), 'MM/YYYY') <> X.COMP
                       AND TRUNC(R.DT_RECEBIMENTO) - TRUNC(MA.DT_VENCIMENTO_ORIGINAL) > 180
                    ), 0)                                                                  AS "NMAIS",
                --
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)
                    +
                NVL((SELECT SUM(A.VL_MENSALIDADE)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)                       AS "TOTAL_CANCELADO",
                --
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE A.NR_MES || '/' || A.NR_ANO = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE <> 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)
                    +
                NVL((SELECT COUNT(A.CD_MENS_CONTRATO)
                     FROM DBAPS.MENS_CONTRATO A
                              INNER JOIN DBAPS.CONTRATO AC ON AC.CD_CONTRATO = A.CD_CONTRATO
                     WHERE TO_CHAR(A.DT_VENCIMENTO_ORIGINAL, 'MM/YYYY') = X.COMP
                       AND AC.TP_CONTRATO <> 'I'
                       AND AC.TP_MENSALIDADE = 'O'
                       AND A.CD_MOTIVO_CANCELAMENTO IS NOT NULL), 0)                       AS "N_TOTAL_CANCELADO"
         FROM (
                  SELECT DISTINCT M.NR_MES || '/' || M.NR_ANO AS "COMP"
                  FROM DBAPS.MENS_CONTRATO M
                  WHERE M.CD_MOTIVO_CANCELAMENTO IS NULL
                    AND M.NR_ANO || M.NR_MES = SUBSTR(('{1}'), 4, 7) || SUBSTR(('{1}'), 0, 2)
-- AND M.NR_ANO || M.NR_MES <= SUBSTR((TO_CHAR(SYSDATE,'MM/YYYY')) ,4,7) || SUBSTR((TO_CHAR(SYSDATE,'MM/YYYY')),0,2)
                    AND TO_NUMBER(M.NR_ANO || M.NR_MES) = {0}
              ) X
         ORDER BY 1
     ) RESUMO
'''


del_13mes_cobranca_pj = '''
DELETE
    FROM analise_cobranca_pj R
    WHERE
        r.DT_COMPETENCIA >= last_day(add_months(trunc(sysdate),- 13) ) +  1'''

ultimos_13_meses = '''select * from CALENDARIO_BI C
WHERE C.PRIMEIRO_DIA_MES <= TO_DATE(SYSDATE,'DD/MM/YY')
and C.PRIMEIRO_DIA_MES >= add_months(trunc( last_day(sysdate)+1),- 13) '''


anterior_a_13_meses = '''
select * from CALENDARIO_BI C
WHERE
      C.PRIMEIRO_DIA_MES >= TO_DATE('01/10/2018','DD/MM/YY')
and trunc(C.PRIMEIRO_DIA_MES) < add_months(trunc( last_day(sysdate)+1),- 13)
'''







del_ant_13mes_cobranca_pj = '''
DELETE
    FROM analise_cobranca_pj R
    WHERE
        r.DT_COMPETENCIA < last_day(add_months(trunc(sysdate),- 13) ) +  1
       '''

