from functions import *
from qry_controle_recebimento import *
from datetime import datetime
import sqlalchemy

# Parametro de conexão com o dw.
parametros = get_parameters()
user_dw = parametros['dw.db']['user']
psw_dw = parametros['dw.db']['pass']
host_dw = parametros['dw.db']['host']

# Parametro de conexão com o banco de produção
user_prdme = parametros['prdme.db']['user']
psw_prdme = parametros['prdme.db']['pass']
host_prdme = parametros['prdme.db']['host']

# Engine do banco DW
engine_prd = get_engine_prdme()
engine_dw = get_engine_dw()

# 1 - Buscar informaçoes dos últimos 13 meses
with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
    df_calendar = pd.read_sql(ultimos_13_meses, connection)

df_calendar = df_calendar.sort_values(by=['ID'], ascending=True)
df_13_mes = pd.DataFrame()
for i, r in df_calendar.iterrows():

    with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
        print('Buscando registro da competencia ', r[3])
        df_13_mes = df_13_mes.append(pd.read_sql(sql_controle_recebimento.format(r[4] + r[5], r[3]), connection))

# 2 - Deletar as informações dos ultimos 13 meses do dw
print('Deletando as informações dos ultimos 13 meses')
with engine_prd.connect() as con:
    con.execute(del_13mes)
print('Deletado')

# 3 -  salvar as informações dos últimos 13 meses no DW
print('Salvando os registros dos últimos 13 meses')
df_13_mes.to_sql('controle_recebimento_bi',
                 con=engine_prd,
                 if_exists='append',
                 index=False,
                 dtype={'COMPETENCIA': sqlalchemy.types.NVARCHAR(length=255)}
                 )
print('Registros salvos')

# 4 - Se domingo
if datetime.today().weekday() == 1:

    # 4.2 - buscar registro entre 10/2018 até   13 meses pra trás
    with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
        df_calendar = pd.read_sql(anterior_a_13_meses, connection)

    df_calendar = df_calendar.sort_values(by=['ID'], ascending=True)
    df_ant_13_mes = pd.DataFrame()
    for i, r in df_calendar.iterrows():
        print('Buscando registro da competencia ', r[3])
        with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
            df_ant_13_mes = df_ant_13_mes.append(pd.read_sql(sql_controle_recebimento.format(r[4] + r[5], r[3]), connection))

    # 4.3 - Deletar registro entre 10/2018 até   13 meses pra trás
    print('Deletando registro entre 10/2018 até   13 meses pra trás')
    with engine_prd.connect() as con:
        con.execute(del_ant_13mes)
    print('deletado')

    # 4.4 - Salvar no DW
    print('Salvando os registros do periodo anterior a 13 meses')
    df_ant_13_mes.to_sql('controle_recebimento_bi', con=engine_prd, if_exists='append', index=False,
                         dtype={'COMPETENCIA': sqlalchemy.types.NVARCHAR(length=255)}
                         )
    print('Registros salvos')

    data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
    data = datetime.strptime(data_execucao, '%d/%m/%Y %H:%M:00')
    with engine_dw.connect() as con:
        con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'controle_receb_ant13mes' ''')
        con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'controle_receb_ant13mes')'''.format(data))

data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'controle_receb_13m' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'controle_receb_13m')'''.format(data))

